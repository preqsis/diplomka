#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy.integrate as integrate
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

class lorenz():
    """
    Reseni Lorenzova atraktoru.
    
    Autor:
    ------
    Bc. Jiri Kveton
    """
    def __init__(self):
        """
        Inicializace a nastaveni parametru vypoctu.
        """
        self.sigma = 10.0
        self.R = 28.0
        self.b = 8.0 / 3.0
    
    def diff_system(self, state, t):
        """
        System diferencialnich rovnic Lorenzova atraktoru.
        """
        dxdt = np.zeros_like(state)
        dxdt[0] = self.sigma*(state[1] - state[0])
        dxdt[1] = self.R*state[0] - state[1] - state[0]*state[2]
        dxdt[2] = state[0]*state[1] - self.b*state[2]
        return dxdt
    
    def solve(self, state, t_start, t_end, dt):
        """
        Provede reseni v zadanem casovem intervalu.
        """
        return integrate.odeint(self.diff_system, state, np.arange(t_start, t_end, dt))
    
    def plot(self, data):
        """
        Vykresli vyslednou krivku Lorenzova atraktoru.
        """
        fig = plt.figure(figsize=(19.2, 10.8))
        ax = fig.gca(projection='3d')
        ax.plot(data[:,0], data[:,1], data[:,2], color="black")
        ax.set_xlabel("$x$", fontsize=25)
        ax.set_ylabel("$y$", fontsize=25)
        ax.set_zlabel("$z$", fontsize=25)
        ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        plt.savefig("lorenzuv_atraktor.png")
        plt.close(fig)

if(__name__ == '__main__'):
    lr = lorenz()
    
    #nastaveni pocatecnich podminek
    init_state = np.array([10.0, 10.0, 10.0])
    
    #reseni Lorenzova atraktoru
    data = lr.solve(init_state, 0.0, 100.0, 0.001)
    
    #vykresleni krivky Lorenzova atraktoru
    lr.plot(data)