#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sympy import *
from sympy.utilities.codegen import codegen
from sympy.utilities.lambdify import lambdify
from scipy.integrate import ode
import numpy as np
import time
import math
import os

class drop_move:
    """
    Simulace modelu kapajiciho kohoutu
    
    Autor:
    ------
    Bc. Jiri Kveton
    """
    def __init__(self):
        """
        Inicializace a nastaveni parametru vypoctu
        """
        # nevyssi pracovni adresar
        self.path = "."
        
        # parametry vypoctu
        self.eta = 0.002
        self.rho = 1.0
        self.Gamma = 1.0
        self.g = 1.0
        self.v0 = 0.01
        self.t_interval = [0.0, 1000.0]
        self.h = 0.01
        
        #povolit sestaveni soustavy rovnic
        self.create_system = True
        
        np.set_printoptions(precision=4, threshold=10, linewidth=200)
    
    def log(self, par, mode):
        """
        Vlozi zaznam do logu
        """
        f = open(self.path+"/log.csv", mode)
        f.write(time.strftime('%Y-%m-%d %H:%M:%S') + "," + str(par["count"]) + "," + str(par["M"]) + "," + str(par["t"]) + "," + par["data_file"] + "\n")
        f.close()
    
    def shift_z0(self, data):
        """
        Posune kapku a polovinu prvniho disku do zapornych hodnot a nastavi z_0 v zapornych
        """
        self.shift = data[1][2] * 0.5
        data[:, 2] = data[:, 2] - self.shift
        
        #zvysit z_0 10x
        data[0][2] = 3.0 * data[0][2]
        self.z0 = data[0][2]
        return data
    
    def data_in(self, target=None):
        """
        Pripravi vstupni data podle souboru data_in.csv
        """
        data = np.genfromtxt(self.path+"/data_in.csv", delimiter=',')
        data = data[::-1,:]
        data = self.shift_z0(data)
        data = np.delete(data, 0, 1)
        data = np.delete(data, 2, 1)
        
        # uchovat polomer kohoutku
        self.ra = data[0][0]
        
        tmp = []
        i = 0
        while(i < len(data)):
            if(i == 0):
                V = 0.0
            else:
                
                if(i == 1):
                    V = np.pi * math.pow(data[0][0], 2) * abs(data[0][1]) + np.pi * data[i][0] * data[i][0] * data[i][1]
                else:
                    V = np.pi * data[i][0] * data[i][0] * (data[i][1] - data[i-1][1])
            
            tmp.append([0.0, data[i][0], data[i][1], self.v0, V])
            i += 1
        
        #ulozit vstupni data
        np.savetxt(self.path+"/data/data_0.csv", np.array(tmp), delimiter=",")
        return len(tmp) - 1
    
    def solve_L(self, data):
        """
        Formuluje rovnice pro vsechny disky a sestavi z nich system
        """
        M = len(data) - 1
        
        # vseobecne symboly
        sym = {}
        sym["g"] = Symbol("g")
        sym["rho"] = Symbol("rho")
        sym["Gamma"] = Symbol("Gamma")
        sym["eta"] = Symbol("eta")
        sym["pi"] = Symbol("pi")
        sym["ra"] = Symbol("ra")
        
        #charakteristiky disku 0
        sym["z0"] = Symbol("z0")
        sym["v0"] = Symbol("v0")
        
        # charakteristiky disku 1 az M
        j = 1
        while(j <= M):
            sym["r"+str(j)] = Symbol("r"+str(j))
            sym["z"+str(j)] = Symbol("z"+str(j))
            sym["v"+str(j)] = Symbol("v"+str(j))
            sym["V"+str(j)] = Symbol("V"+str(j))
            j += 1
        
        par = []
        for key in sym:
            par.append(key)
        
        #celkova kin. energie kapky
        E_kin = 0
        j = 1
        while(j <= M):
            E_kin += 0.5 * sym["rho"] * sym["V"+str(j)] * (sym["v"+str(j)])**2
            j += 1
        
        #celkova pot. energie kapky
        E_pot = 0
        j = 1
        while(j <= M):
            E_pot += ( -1.0 * sym["g"] * sym["rho"] * sym["V"+str(j)] * sym["z"+str(j)])
            
            j += 1
        
        #celkova pov. energie kapky
        S = 0
        j = 1
        while(j <= M):
            if(j == M):
                rj = sqrt( sym["V"+str(j)] / (sym["pi"] * (sym["z"+str(j)] - sym["z"+str(j-1)])) )
                S += sym["pi"] * rj**2
            else:
                rjp1 = sqrt( sym["V"+str(j+1)] / (sym["pi"] * (sym["z"+str(j+1)] - sym["z"+str(j)])) )
                if(j == 1):
                    rj = sqrt( (sym["V"+str(j)] - sym["pi"] * sym["ra"]**2 * Abs(sym["z0"]) ) / (sym["pi"] * (sym["z"+str(j)])) )
                    S += sym["pi"] * (rj + sym["ra"]) * sqrt((rj - sym["ra"])**2 + sym["z1"]**2)
                    S += sym["pi"] * 0.5 * (3*rj + rjp1) * sqrt(0.25*(rjp1-rj)**2 + 0.25*(sym["z2"] - sym["z1"])**2)
                else:
                    rj = sqrt( sym["V"+str(j)] / (sym["pi"] * (sym["z"+str(j)] - sym["z"+str(j-1)])) )
                    S += sym["pi"] * (rj + rjp1) * sqrt( 0.25 * (sym["z"+str(j+1)] - sym["z"+str(j-1)])**2 + (rj - rjp1)**2 )
                
            j += 1
        E_pov = sym["Gamma"] * S
        
        #Lagrangian
        L = E_kin - E_pot - E_pov
        
        #disipacni funkce
        dsp = 0
        j = 1
        while(j <= M):
            dsp += -3.0 * sym["eta"] * (( sym["v"+str(j)] - sym["v"+str(j-1)] )**2 / ( sym["z"+str(j)] - sym["z"+str(j-1)] )**2) * sym["V"+str(j)] * sym["rho"]
            j += 1
        
        #pohybove pro vsechny disky z1 az zM
        self.system = {}
        j = 0
        while(j <= M):
            if(j == 0):
                aj = 0.0
            else:
                aj = ( diff(L, sym["z"+str(j)]) + 0.5 * diff(dsp, sym["v"+str(j)]) ) / (sym["V"+str(j)] * sym["rho"])
            self.system["a"+str(j)] = lambdify(par, aj)
            j += 1
        self.create_system = False
    
    def solve(self, y0, t, par):
        """
        Provede numericke reseni systemu
        """
        #reseni
        sol = ode(self.diff_system).set_integrator("dopri5", nsteps=10000)
        sol.set_initial_value(y0, t).set_f_params(par)
        sol.integrate(t+self.h)
        
        #pretvarovat vysledek na 2 sloupce
        i = 0
        y1 = []
        while(i < len(sol.y)):
            y1.append([sol.y[i], sol.y[i+1]])
            i += 2
        
        return np.array(y1)
    
    def diff_system(self, t, y0, par):
        """
        System diff. rovnic k reseni
        """
        system = []
        
        i = 0
        while(i < len(y0) / 2):
            par["z"+str(i)] = y0[2*i]
            par["v"+str(i)] = y0[2*i + 1]
            i += 1
        
        j = 0
        for a in self.system:
            #dz/dt = v
            system.append(par["v"+str(j)])
            
            #dv/dt = a
            system.append(self.system["a"+str(j)](**par))
            
            j += 1
        return np.array(system)
    
    def disc_increase(self, data):
        """
        Postupne zvysuje pocet disku kapky podle pritoku od kohoutku
        """
        tmp = []
        V_b = data[1][4] - np.pi * math.pow(self.ra, 2) * abs(data[0][2])
        if(V_b < 0.01):
            return data
        else:
            tmp = []
            t = data[0][0]
            z_0 = self.z0
            z_1 = data[1][2] * 0.3
            z_2 = data[1][2]
            r_2 = data[1][1]
            V_2 = np.pi * r_2 * r_2 * (z_2 - z_1)
            V_b1 = V_b - V_2
            V_1 = np.pi * math.pow(data[0][1], 2) * abs(z_0) + V_b1
            r_1 = math.sqrt(V_b1 / (np.pi * z_1))
            v_1 = self.v0
            v_2 = data[1][3]
            tmp.append([t, self.ra, z_0, self.v0, 0.0])
            tmp.append([t, r_1, z_1, v_1, V_1])
            tmp.append([t, r_2, z_2, v_2, V_2])
            j = 2
            
            while(j < len(data)):
                tmp.append(data[j])
                j = j + 1
            
            self.create_system = True
            return np.array(tmp)
    
    def disc_divide(self, data):
        """
        Provede rozdeleni prilis roztazenych disku na polovinu
        """
        tmp = []
        
        tmp.append(data[0])
        tmp.append(data[1])
        tmp.append(data[2])
        
        i = 3
        while(i < len(data)-1):
            #vzdelenost sousednich bodu
            dl = math.sqrt( math.pow(data[i][1]-data[i-1][1], 2) + math.pow(data[i][2]-data[i-1][2], 2) )
            
            #print dl
            
            if(dl > 0.1):
                V_1 = data[i-1][4]
                V_2 = data[i][4]
                V_3 = data[i+1][4]
                r_1 = data[i-1][1]
                r_2 = data[i][1]
                r_3 = data[i+1][1]
                z_1 = data[i-1][2]
                z_2 = data[i][2]
                z_3 = data[i+1][2]
                v_1 = data[i-1][3]
                v_2 = data[i][3]
                v_3 = data[i+1][3]
                r_21 = (r_1 + r_2) / 2.0
                r_22 = r_2
                z_21 = (z_1 + z_2) / 2.0
                z_22 = z_2
                dv = (v_3 - v_1) / (z_3 - z_1)
                v_21 = dv * (z_21 - z_1) + v_1
                v_22 = v_3 - dv * (z_3 - z_22)
                V_22 = np.pi * math.pow(r_22, 2) * (z_22 - z_21)
                V_21 = V_2 - V_22
                tmp.append([data[i][0], r_21, z_21, v_21, V_21])
                tmp.append([data[i][0], r_22, z_22, v_22, V_22])
            else:
                tmp.append(data[i])
            i += 1
        tmp.append(data[len(data)-1])
        
        self.create_system = True
        return np.array(tmp)
    
    def data_out(self, data, tmp):
        """
        Zpracuje vystupni data z reseni do formatu pro ulozeni
        """
        data_out = []
        t = data[0][0] + self.h
        
        i = 0
        while(i < len(tmp)):
            data_out.append([t, data[i][1], tmp[i][0], tmp[i][1], data[i][4]])
            i += 1
        
        return data_out
    
    def solve_init(self, data):
        """
        Pripravi sadu vsupnich parametru a pocatecni vektor do reseni
        """
        y0 = []
        par = {}
        
        #pripravit init vektor
        i = 0
        while(i < len(data)):
            y0.append(data[i][2])
            y0.append(data[i][3])
            i += 1
        y0 = np.array(y0)
        
        #dalsi parametry pro reseni
        par["ra"] = self.ra
        par["pi"] = np.pi
        par["eta"] = self.eta
        par["Gamma"] = self.Gamma
        par["rho"] = self.rho
        par["g"] = self.g
        par["z0"] = data[0][2]
        par["v0"] = self.v0
        
        j = 1
        while(j < len(data)):
            par["r"+str(j)] = data[j][1]
            par["V"+str(j)] = data[j][4]
            j += 1
        
        return y0, par
    
    def renew_rj(self, data):
        i = 1
        while(i < len(data)):
            if(i == 1):
                if(data[i][2] == 0.0):
                    data[i][1] = self.ra
                else:
                    data[i][1] = math.sqrt( (data[i][4] - np.pi * self.ra * self.ra * abs(data[0][2]) ) / (np.pi * data[i][2]))
            else:
                data[i][1] = math.sqrt( data[i][4] / ( np.pi * (data[i][2] - data[i-1][2]) ) )
            i += 1
        
        return data
    
if(__name__ == "__main__"):
    solver = drop_move()
    
    #pokud existuje zaznam o simulaci tak zavazat jinak vytovit novou
    if(os.path.isfile(solver.path+"/log.csv")):
        log = np.genfromtxt(solver.path+"/log.csv", delimiter=",")
        i = len(log)-1
        count = int(log[i][1])
        data_0 = np.genfromtxt(solver.path+"/data/data_0.csv", delimiter=",")
        solver.z0 = data_0[0][2]
        data = np.genfromtxt(solver.path+"/data/data_"+str(count)+".csv", delimiter=",")
        solver.ra = data[0][1]
        t = float(data[len(data)-1][0])
    else:
        M = solver.data_in()
        solver.log({"count":0, "M":M, "data_file":"data_0.csv", "t":0.0}, "w")
        count = 0
        t = solver.t_interval[0]
    
    while(t <= solver.t_interval[1]):
        
        #nacist data
        data = np.genfromtxt(solver.path+"/data/data_"+str(count)+".csv", delimiter=',')       
        
        #sestavit system rovnic (jen pokud je to poprve nebo pokud se pocet rovnic zmenil)
        if(solver.create_system):
            solver.solve_L(data)
        
        #ziskat vstupni parametry do reseni
        y0, par = solver.solve_init(data)
        
        # reseni systemu o jeden krok
        tmp = solver.solve(y0, t, par)
        t += solver.h
        count += 1
        
        #zpracovat vystupni data
        data_out = solver.data_out(data, tmp)
        
        #zvysovani poctu disku pritokem z kohoutku
        data_out = solver.disc_increase(data_out)
        
        #deleni prilis roztazenych disku
        data_out = solver.disc_divide(data_out)
        
        #prepocet rj na aktualni hodnoty
        data_out = solver.renew_rj(data_out)
        
        #ulozit data
        np.savetxt(solver.path+"/data/data_"+str(count)+".csv", np.array(data_out), delimiter=",")
        
        #zapis do logu
        solver.log({"count":count, "M":len(data_out), "data_file":"data_"+str(count)+".csv", "t":t}, "a")
        
        #informacni hlaska
        print("t = " + str(t) + ", M = " + str(len(data_out)))