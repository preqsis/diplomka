#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import scipy as sp
import numpy as np
from scipy.integrate import ode

class mass_spring:
    """
    Mass-Spring model kapajiciho kohoutku.
    
    Autor:
    ------
    Bc. Jiri Kveton
    """
    def __init__(self):
        """
        Inicializace a nastaveni parametru simulace.
        """
        #parametry modelu
        self.ra = 0.916
        self.m0 = 4.61
        self.z0 = 2.0
        self.g = 1.0
        self.gamma = 0.05
        self.z_crit = 5.5
        
        #datove containery
        self.drop_data = [] #data o pohybu kapky [t, z, v, m, k]
        self.break_data = [] #data odtrzeni [t, z, v, m, k]
    
    def get_k(self, m):
        """
        Urceni tuhosti pruziny podle aktualni hmotnosti.
        """
        if(m < 4.61):
            return -11.4 * m + 52.5
        else:
            return 0.0
    
    def diff_system(self, t, y, p):
        """
        System diferencialnich rovnic Mass-Spring modelu.
        """
        m = p[0]
        k = p[1]
        v = y[1]
        a = self.g - k * y[0] / m - self.gamma * y[1] / m - (np.pi * math.pow(self.ra, 2) * self.v0 * (y[1] - self.v0)) / m
        return np.array([v, a])
    
    def solve(self, v0=0.1, t_start=0.0, t_end=1000.0, h=0.01):
        """
        Provede reseni v urcenem casovem intervalu s predem nastavenymi parametry.
        """
        self.v0 = v0
        t = t_start
        m = self.m0
        y = np.array([self.z0, self.v0])
        
        while(t <= t_end):
            k = self.get_k(m)
            
            sol = ode(self.diff_system).set_integrator("dop853")
            sol.set_initial_value(y, t).set_f_params([m, k])
            sol.integrate(t + h)
            
            y = sol.y
            t += h
            m += np.pi * math.pow(self.ra, 2) * self.v0 * h
            
            data = [t , y[0], y[1], m, k]
            self.drop_data.append(data)
            
            #kontrola odtrzeni --> reset parametru
            if(y[0] >= self.z_crit):
                
                self.break_data.append(data)
                
                print data
                
                m = 0.2 * m + 0.3
                y[0] = 2.0
                y[1] = 0.0
        
        self.drop_data = np.array(self.drop_data)
        self.break_data = np.array(self.break_data)
        
        np.savetxt("drop_data_"+str(self.v0)+".csv", self.drop_data, delimiter=",")
        np.savetxt("break_data_"+str(self.v0)+".csv", self.break_data, delimiter=",")

if(__name__ == "__main__"):
    ms = mass_spring()
    
    #simulace se zadanou rychlosti pritoku v0
    ms.solve(v0=0.146)