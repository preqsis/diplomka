#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import ode

class ode_compare:
    """
    Srovnani presnosti numerickych metod reseni obycejnych diferencialnich rovnic.
    
    Autor:
    ------
    Bc. Jiri Kveton
    """
    def __init__(self):
        """
        Inicializace a nastaveni parametru vypoctu.
        """
        self.h = 0.1
    
    def euler(self, x0, y0):
        """
        Eulerova metoda.
        """
        x = x0
        y = y0
        tmp = [[x, y]]
        while(x < 7.9):
            y += self.h * self.func(x, y)
            x += self.h
            tmp.append([x, y])
        return np.array(tmp)
    
    def rk4(self, x0, y0):
        """
        Runge-Kuttova metoda 4-teho radu.
        """
        x = x0
        y = y0
        tmp = [[x, y]]
        while(x < 7.9):
            k1 = self.func(x, y)
            k2 = self.func(x+0.5*self.h, y+0.5*self.h*k1)
            k3 = self.func(x+0.5*self.h, y+0.5*self.h*k2)
            k4 = self.func(x+self.h, y+self.h*k3)
            y += (1.0 / 6.0) * self.h * (k1 + 2.0 * k2 + 2.0 * k3 + k4) 
            x += self.h
            tmp.append([x, y])
        return np.array(tmp)
    
    def dopri(self, x0, y0):
        """
        Dormand-Princova metoda.
        """
        x = x0
        y = y0
        tmp = [[x, y]]
        while(x < 7.9):
            sol = ode(self.func).set_integrator("dopri5", nsteps=10000)
            sol.set_initial_value(y, x)
            sol.integrate(x+self.h)
            y = sol.y
            x += self.h
            tmp.append([x, y])
        return np.array(tmp)
    
    def func(self, x, y):
        """
        Srovnavaci rovnice.
        """
        return math.exp(x) + y * math.cos(2.0 * y) + math.sin(x)
    
    def plot(self, data_1, data_2, data_3):
        """
        Graf srovnani vysledku.
        """
        fig = plt.figure(figsize=(19.2, 10.8))   
        ax = fig.add_subplot(1,1,1)
        plt.subplots_adjust(top=0.95, bottom=0.05, hspace=0.08, left=0.05, right=0.95)
        ax.plot(data_1[:, 0], data_1[:, 1], color="blue", linewidth=1.0, marker="o")
        ax.plot(data_2[:, 0], data_2[:, 1], color="red", linewidth=1.0, marker="o")
        ax.plot(data_3[:, 0], data_3[:, 1], color="green", linewidth=1.0, marker="o")
        ax.set_xlim([0.0, 8.5])
        ax.set_ylim([-100.0, 4000.0])
        ax.set_xlabel("$x$", fontsize=30)
        ax.set_ylabel("$y$", fontsize=30)
        ax.legend(["Eulerova metoda", "Runge-Kuttova metoda 4-teho radu", "Dormand-Princova metoda"], loc=2, fontsize=25)
        ax.grid()
        plt.savefig("ode_srovnani.png")
        plt.close(fig)
    
if(__name__ == '__main__'):
    comp = ode_compare()
    
    #reseni s pouzitim ruznych metod
    data_1 = comp.euler(0.0, 1.0)
    data_2 = comp.rk4(0.0, 1.0)
    data_3 = comp.dopri(0.0, 1.0)
    
    #graf srovnani vysledku
    comp.plot(data_1, data_2, data_3)