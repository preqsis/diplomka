#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
import scipy as sc
import math as mt
import matplotlib.pyplot as plt

class logistic_function():
    """
    Zpracovani bifurkacniho diagramu logisticke funkce.
    
    Autor:
    ------
    Bc. Jiri Kveton
    """
    def __init__(self):
        """
        Inicializace a nastaveni parametru vypoctu.
        """
        self.dr = 0.001
    
    def solve(self, r=2.6, x0=0.5, count=400):
        """
        Reseni zadaneho poctu kroku pro jednu hodnotu r.
        """
        tmp = []
        x = x0
        i = 1
        while(i <= count):
            x = r * x * (1 - x)
            tmp.append([r, x])
            i += 1
        return np.array(tmp)
    
    def plot_bifurcation(self):
        """
        Vykresleni bifurkacniho diagramu z vypoctenych dat.
        """
        files = os.listdir("data")
        data = np.array([[0, 0]])
        
        for f in files:
            data = np.concatenate((data, np.genfromtxt("data/"+f, delimiter=",")[250:]), axis=0)
        data = np.delete(data, 0, 0)
        
        fig = plt.figure(figsize=(19.2, 10.8))
        ax = fig.gca()
        ax.plot(data[:,0], data[:,1], marker="o", markersize=0.6, linestyle="")
        ax.set_xlabel("$r$", fontsize=30.0)
        ax.set_ylabel("$x_n$", fontsize=30.0)
        ax.set_xlim(2.7, 4.0)
        ax.set_ylim(-0.05, 1.05)
        ax.grid()
        plt.savefig("logisticka_funkce.png")

if(__name__ == '__main__'):
    lm = logistic_function()
    
    #reseni v intervalu r
    r = 2.7
    while(r <= 4.0):
        print "r = "+str(r)
        data = lm.solve(r=r)
        np.savetxt("data/data_"+str(r)+".csv", data, delimiter=",")
        r += lm.dr
    
    #vykreslit bifurkacni diagram
    lm.plot_bifurcation()