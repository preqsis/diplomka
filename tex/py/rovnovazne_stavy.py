#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import os
import time
import re
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import ode
from sympy import *
from sympy.utilities.lambdify import lambdify

class shapes:
    """
    Reseni rovnovaznych tvaru visicich kapek.
    
    Autor:
    ------
    Bc. Jiri Kveton
    """
    def __init__(self):
        """
        Inicializace a nastaveni parametru vypoctu.
        """
        #nejvyssi pracovni adresar
        self.path = "."
        
        #parametry vypoctu
        self.P_range = [0.01, 7.0] # interval P
        self.P_step = 0.01 # krok P
        self.s = sp.linspace(0.0, 8.0, 2E2) # interval s
        self.r0 = 1E-20 # pocatecni polomer
        self.th0 = math.radians(90) # pocatecni uhel tecny s osou z
        
        #inicializace pracovniho adresare data
        if(self.check_dir(self.path+"/data")):
            self.clear_dir(self.path+"/data")
        
        #inicializace pracovniho adresare plot
        if(self.check_dir(self.path+"/plot")):
            self.clear_dir(self.path+"/plot")
        
    def check_dir(self, dir_path):
        """
        Zkontroluje existenci zadaneho adresare, pokud neexistuje tak ho vytvori.
        """
        if(os.path.isdir(dir_path) == False):
            os.mkdir(dir_path)
            return False
        else:
            return True
    
    def clear_dir(self, dir_path):
        """
        Vymaze obsah zadaneho adresare.
        """
        os.system("rm -R "+dir_path+"/*")
    
    def diff_system(self, init_vector, s):
        """
        Soustava diff. rovnic popisujici tvar kapky, r(0) = 0, z(0) = Pb tzn. tlak na spodu kapky, th(0) = pi / 2 .
        """
        r0 = init_vector[0]
        z0 = init_vector[1]
        th0 = init_vector[2]
        return sp.array([math.sin(th0),  (-1.0) * math.cos(th0), (math.cos(th0) / r0) - z0])
    
    def solve(self):
        """
        Provede reseni tvaru kapek v zadanem rozsahu.
        """
        P = self.P_range[0]
        n = 1
        
        #vystupni adresar nenavazanych tvaru
        self.check_dir(self.path+"/data/shape")
        
        #reseni tvaru v rozsahu P 
        while(P <= self.P_range[1]):
            print "shape: P = "+str(P)
            
            # reseni
            init_vector = sp.array([self.r0, P, self.th0])
            data, infodict = sp.integrate.odeint(self.diff_system, init_vector, self.s, full_output=True)
            
            # ulozit data
            np.savetxt(self.path+"/data/shape/shape_"+self.lead_zero(n, 4)+"_"+self.end_zero(P, 5)+".csv", np.insert(data, 0, self.s, axis=1), delimiter=",")
            
            P += self.P_step
            n += 1
    
    def lead_zero(self, num, n):
        """
        Doplni retezec na pozadovanou delku nulami na zacatku.
        """
        num = str(num)
        while(len(num) < n):
            num = "0" + num
        return num
    
    def end_zero(self, num, n):
        """
        Doplni retezec na pozadovanou delku nulami na konci.
        """
        num = str(num)
        while(len(num)<n):
            num = num + "0"
        return num
    
    def bound_ceiling(self):
        """
        Urci mozne vazby kapek na kohoutek.
        """
        import re
        
        #vystupni adresar navazanych tvaru
        self.check_dir(self.path+"/data/shape_ceiling")
        
        #nalezt mozne vazby pro vsechny tvary
        for file_name in os.listdir(self.path+"/data/shape"):  
            out_name = re.sub("\.csv", "", file_name)
            out_name = re.sub("original", "ceiling", out_name)
            
            P = out_name.split("_")[2]
            data = np.genfromtxt(self.path+"/data/shape/"+file_name, delimiter=',')
            
            print "bound to ceiling: P = "+str(P)+" file = "+str(file_name)
            
            tmp = []
            
            i = 0
            while(data[i][2] >= 0.0):
                tmp.append(data[i])
                i += 1
            
            np.savetxt(self.path+"/data/shape_ceiling/"+out_name+".csv", np.array(tmp), delimiter=",")
    
    def bound_faucet(self, r_faucet=1):
        """
        Urci mozne vazby kapek na kohoutek.
        """
        import re
        
        #vystupni adresar navazanych tvaru
        self.check_dir(self.path+"/data/shape_faucet")
        
        #nalezt mozne vazby pro vsechny tvary
        for file_name in os.listdir(self.path+"/data/shape"):  
            out_name = re.sub("\.csv", "", file_name)
            out_name = re.sub("original", "faucet", out_name)
            
            P = out_name.split("_")[2]
            data = np.genfromtxt(self.path+"/data/shape/"+file_name, delimiter=',')
            
            print "bound to faucet: P = "+str(P)+" file = "+str(file_name)
            
            for line in data:
                if(line[1] >= r_faucet):
                    z1 = line[2]
                    break
                else:
                    z1 = 0.0
            for line in data:
                if(line[1] <= r_faucet and line[2] < z1):
                    z2 = line[2]
                    break
                else:
                    z2 = 0.0
            
            data_1 = []
            data_2 = []
            data_3 = []
            
            for line in data:
                if(line[1] <= r_faucet and line[2] >= z1):
                    data_1.append(line)
                elif(line[1] > r_faucet and line[2] <= z1 and line[2] >= z2):
                    data_2.append(line)
                elif(line[1] <= r_faucet and line[2] <= z2 and line[2] >= 0):
                    data_3.append(line)
            
            tmp_1 = np.array(data_1)
            tmp_1, shift_1 = self.shift_to_zero(tmp_1)
            np.savetxt(self.path+"/data/shape_faucet/"+out_name+"_1.csv", tmp_1, delimiter=",")
            if(z2 != 0):
                tmp_2 = np.array(data_1+data_2)
                tmp_3 = np.array(data_1+data_2+data_3)
                
                tmp_2, shift_2 = self.shift_to_zero(tmp_2)
                tmp_3, shift_3 = self.shift_to_zero(tmp_3)
                
                np.savetxt(self.path+"/data/shape_faucet/"+out_name+"_2.csv", tmp_2, delimiter=",")
                np.savetxt(self.path+"/data/shape_faucet/"+out_name+"_3.csv", tmp_3, delimiter=",")
    
    def shift_to_zero(self, data):
        """
        Posune kapku po vazbe k nule
        """
        shift = data[len(data) - 1][2]
        data[:, 2] = data[:, 2] - shift
        return data, shift
    
    def volume(self):
        """
        Vypocte objemy vsech kapek
        """
        import re
        
        data_out = []
        for file_name in os.listdir(self.path+"/data/shape_faucet"):
            P = float(re.sub(r"\.csv", "", file_name).split("_")[2])
            
            print "volume: P = "+str(P)+" file = "+str(file_name)
            
            data = np.genfromtxt(self.path+"/data/shape_faucet/"+file_name, delimiter=',')
            
            r = data[:, 1]
            z = data[:, 2]
            
            V = 0.0
            i = len(r)-1
            while(i>0):
                if(z[i] >= 0):
                    V = V + math.pi * math.pow(r[i], 2) * (z[i-1]-z[i])
                i = i-1
            data_out.append([P, V])
        
        data_out = np.array(data_out)
        np.savetxt(self.path+"/data/volume.csv", data_out, delimiter=",")
    
    def plot_volume(self):
        """
        Plot grafu V = f(P)
        """
        print "plot: V = f(P)"
        
        data = np.genfromtxt(self.path+"/data/volume.csv", delimiter=',')
        fig = plt.figure(figsize=(19.2, 10.8))   
        ax1 = fig.add_subplot(1,1,1)
        ax1.plot(data[:, 0], data[:, 1], marker="o", markersize=3, linestyle="", color="blue")
        ax1.set_xlabel("$P_{b}$")
        ax1.set_ylabel("$V$")
        ax1.grid()
        plt.savefig(self.path+"/plot/volume_P_faucet.png")
        plt.close(fig)
    
    def critical_volume(self):
        """
        Nalezne kapku s kritickou hodnotou objemu a zkopiruje soubor na vstup do simulace vyvoje kapky
        """
        data = np.genfromtxt(self.path+"/data/volume.csv", delimiter=',')
        crit = data[np.where(data[:,1] == np.max(data[:,1]))]
        print crit
        os.system("cp "+self.path+"/data/shape_faucet/shape_*_"+str(crit[0][0])+"_1.csv "+self.path+"/../move/data_in.csv")

if(__name__ == "__main__"):
    sh = shapes()
    
    #provest reseni
    sh.solve()
    
    #urcit vazbu na strop
    sh.bound_ceiling()
    
    #urcit mozne vazby na kohoutek v zadanem polomeru
    sh.bound_faucet(r_faucet=0.5)
    
    #vypocitat objemy kapek
    sh.volume()
    
    #vykreslit graf funkce V = f(P)
    sh.plot_volume()
    
    #nalezt kapku s kritickym objemem a zkopirovat soubor na vstup do simulace vyvoje kapky
    sh.critical_volume()
