\documentclass[xcolor=dvipsnames, 9pt]{beamer}

%
% NASTAVENÍ BAREV A TÉMATU
%
\definecolor{Prf}{rgb}{.06,.7,.35}
\definecolor{Muni}{rgb}{.06,.22,.51}
\setbeamercolor{title}{fg=black}
\setbeamercolor{enumerate item}{fg=black}
\setbeamercolor{itemize item}{fg=black}
\setbeamercolor{itemize subitem}{fg=black}
\usecolortheme[named=Prf]{structure} 
\usetheme{Malmoe}

%
% NASTAVENÍ FONTU
%
%\usepackage[T1]{fontenc}
%\usepackage[bitstream-charter]{mathdesign}

%
% DALŠÍ POUŽITÉ BALÍČKY
%
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{color}
\usepackage{lipsum}
\usepackage{url}
\usepackage{natbib}
\usepackage[labelfont=bf]{caption}
\usepackage{mathrsfs}
\usepackage{tabularx}
\usepackage{microtype}

\begin{document}

%
% TITULNÍ STRANA
%
\title{Nelineární procesy v akrečních discích}
\subtitle{DIPLOMOVÁ PRÁCE}
\author{Bc. Jiří Květoň}
\institute{Ústav teoretické fyziky a astrofyziky}
\date{16. 6. 2014}
\titlegraphic
{
	\includegraphics[width=3cm]{img/znak_MU_modry.eps}
	\hspace*{2cm}
   \includegraphics[width=3cm]{img/znak_PrF_P354.eps}
}

\maketitle

\begin{frame}{Cíle práce}
\begin{enumerate}
  \item Vytvořit jednoduché nelineární modely kapajícího kohoutku, které v modifikované podobě mohou být jedním z možných vysvětlení nepravidelného chování akrečních disků.
  \item Z výsledků simulací těchto modelů získat některé charakteristické invarianty systému kapajícího kohoutku.
  \item Navrhnout modifikaci vytvořených modelů do podoby \textit{modelu kapajícího disku}. 
\end{enumerate}
\end{frame}

\begin{frame}{Modely kapajícího kohoutku}

Vytvořeny a popsány byly dva modely kapajícího kohoutku.

\hspace{20mm}

\begin{enumerate}
	\item \textbf{Dynamický kapalinový model} ({\scriptsize FDM z angl. \textit{Fluid Dynamical Model}})
	\begin{itemize}
		\item Kapka je modelována jako diskrétní sada vzájemně vázaných disků pod vlivem gravitace na základě Lagrangeových rovnic.
		\item Vhodnější ke studiu procesů vedoucích k odtržení kapky a následně přesnějšímu nastavení parametrů MSMM.
		\item Nevýhodou je vyšší výpočetní náročnost.
	\end{itemize}
	\item \textbf{Modifikovaný pružinový model} ({\scriptsize MSMM z angl. \textit{Mass--Spring Model Modified}})
	\begin{itemize}
		\item Kapka je modelována jako závaží s měnící se hmotností na pružině. K odkápnutí dochází po dosažení zvolených kritických parametrů.
		\item Vhodnější ke studiu dlouhodobého chování kapajícího kohoutku a hledání podmínek, při kterých systém vykazuje chaotické chování.
		\item Je nutné dobře zvolit parametry modelu (například podle výsledků FDM).
		\item Výhodou je nižší výpočetní náročnost.
	\end{itemize}
\end{enumerate}

\hspace{20mm}

Hlavním řídícím parametrem obou modelů je rychlost přítoku kapaliny z kohoutku $v_0$.
\end{frame}

\begin{frame}{FDM - definice systému}

Proměnné v systému visící kapky jsou definovány jako

\begin{center}
	\includegraphics[width=5cm]{img/definice_rovnovazne.png}
\end{center}

Řešení prováděno v CGS a díky vhodně zvoleným základním jednotkám platí $g = \rho = \Gamma = 1$. Tyto základní jednotky jsou definovány jako

\begin{equation*}
     \begin{aligned}
       		l_0 &\equiv \sqrt{\frac{\Gamma}{\rho g}} =  0{,}27 \textrm{cm}\\
       		m_0 &\equiv \rho l_0^3 = 0{,}02 \textrm{g} \\
       		P_0 &\equiv \sqrt{\rho g \Gamma} = 270 \textrm{dyn} \cdot \textrm{cm}^{-2} \\
       		t_0 &= 0{,}017 \textrm{s}. 
     \end{aligned}
     \label{jednotky}
\end{equation*}

\end{frame}

\begin{frame}{FDM - řešení rovnovážných tvarů visících kapek}

Pro různé hodnoty $P_b$ získáme rovnovážné tvary visících kapek, které nejprve omezíme pouze rovinou $z = 0$ (tzv. \textit{kapky na stropě}).

\begin{center}
	\includegraphics[width=11cm]{img/shapes_ceiling_set.png}
\end{center}

\end{frame}

\begin{frame}{FDM - řešení rovnovážných tvarů visících kapek}

Ze závislosti $V = f(P_b)$ vidíme, že stabilní (realizovatelné) tvary kapek \textit{na stropě} jsou pouze v intervalu $P_b \in \langle 0; 1,72 \rangle$.

\begin{center}
	\includegraphics[width=11cm]{img/volume_P_ceiling.png}
\end{center}

\end{frame}

\begin{frame}{FDM - řešení rovnovážných tvarů visících kapek}

Pro různé hodnoty $P_b$ získáme rovnovážné tvary visících kapek, které omezíme rovinou $z = 0$ a poloměrem kohotku $r_a = 0,952$.

\begin{center}
	\includegraphics[width=11cm]{img/shapes_faucet_set.png}
\end{center}

\end{frame}

\begin{frame}{FDM - řešení rovnovážných tvarů visících kapek}

Ze závislosti $V = f(P_b)$ vidíme, že stabilní (realizovatelné) tvary kapek \textit{na kohoutku} jsou pouze v intervalu $P_b \in \langle 0; 2,67 \rangle$.

\begin{center}
	\includegraphics[width=11cm]{img/volume_P_faucet.png}
\end{center}

\end{frame}

\begin{frame}{FDM - vývoj systému v čase}

Jako vstupní data zvolíme rovnovážný tvar těsně pod hranicí stability a kapku rozdělíme na sadu disků, pro které bude platit $\Delta s = \textrm{konst.}$

\hspace{38.5mm}
\includegraphics[width=5cm]{img/fdm_vysvetlujici.png}

\end{frame}

\begin{frame}{FDM - výsledky simulace}
	\begin{center}
		\includegraphics[width=11cm]{img/multiplot.png}
	\end{center}
\end{frame}

\begin{frame}{MSMM - vývoj systému v čase}

Pohyb těžiště v MSMM je popsán soustavou rovnic

\begin{equation*}
	\begin{aligned}
		& \dot{z} = v~\\
     	& \dot{v} = g - \frac{1}{m} \left[ kz + \gamma v~+ (v~- v_0) Q \right],
     \end{aligned}
     \label{msm_4}
\end{equation*}

kterou lze řešit numericky s využitím Dormand--Princovy metody s adaptivním krokem.

\end{frame}

\begin{frame}{MSMM - intervaly odtržení}

Při simulaci časového vývoje MSMM sledujeme intervaly mezi po sobě jdoucími odtrženími kapky a chování systému v závislosti na nastavené rychlosti přítoku kapaliny $v_0$.

	\begin{center}
		\includegraphics[width=11cm]{img/T_srovnani.png}
	\end{center}

\end{frame}

\begin{frame}{MSMM - bifurkační diagram}

Provedením simulací ve zvoleném rozsahu rychlostí přítoku $v_0$ získáme bifurkační diagram pro intervaly odtržení $T_n$ v závislosti na bifurkačním parametru $v_0$.

\includegraphics[width=11cm]{img/plot_bif.png}


\end{frame}

\begin{frame}{MSMM - zobrazení do stavového prostoru}

Výsledky simulace například pro $v_0 = 0,146$ zobrazíme do stavového prostoru souřadnic $z$, $\dot{z}$ a $m$.

	\begin{center}
		\includegraphics[width=11cm]{img/phase_space_0146.png}
	\end{center}

\end{frame}

\begin{frame}{MSMM - zobrazení do stavového prostoru}

Pro lepší představu o tvaru stavového portrétu můžeme zobrazit pouze jeho průměty do rovin $\dot{z} = 0$ a $z = 0$.

	\begin{center}
		\includegraphics[width=11cm]{img/phase_space_2d.png}
	\end{center}

\end{frame}

\begin{frame}{Model kapajícího disku z MSMM}

MSMM můžeme modifikovat do podoby prstence složeného z $N$ buněk, u kterých přítok hmoty bude vyjádřen jako součet konstantní akrece $A$ a difuze $D$ závislé na gradientu hmoty mezi sousedními buňkami.

	\begin{center}
		\includegraphics[width=10cm]{img/ring_definice.png}
	\end{center}

\end{frame}

\begin{frame}{Model kapajícího disku z MSMM}

 Soustava rovnic popisující model kapajícího disku má tvar

	\begin{equation*}
	\begin{aligned}
		\dot{z}_1 &= v_1 \\
     	\dot{v}_1 &= g - \frac{1}{m_1} \left[ kz_1 + \gamma v_1 + v_1 A~+ v_1 D(m_{N}, m_{1}, m_{2})  \right] \\
     	...\\
     	\dot{z}_i &= v_i \\
     	\dot{v}_i &= g - \frac{1}{m_i} \left[ kz_i + \gamma v_i + v_i A~+ v_i D(m_{i-1}, m_{i}, m_{i+1})  \right] \\
     	...\\
     	\dot{z}_N &= v_N \\
     	\dot{v}_N &= g - \frac{1}{m_N} \left[ kz_N + \gamma v_N + v_N A~+ v_N D(m_{N-1}, m_{N}, m_{1})  \right].
     \end{aligned}
     \label{msm_disk_4}
	\end{equation*}
\end{frame}

\begin{frame}{Závěr}

\textbf{Dosažené výsledky:}

\begin{itemize}
	\item Podařilo se popsat a otestovat model FDM, který lze využít ke studiu procesů formování kapek.
	\item Podařilo se popsat a otestovat model MSMM a z výsledků simulací tohoto modelu byly určeny některé charakteristické invarianty.
	\item Byl předložen návrh modifikace modelu MSMM do podoby modelu kapajícího disku.
\end{itemize}

\textbf{Práce do budoucna:}

\begin{itemize}
 \item Optimalizace programové implementace modelu FDM pro snížení výpočetní náročnosti modelu.
 \item Nalezení dostatečně přesného tvaru difuzní funkce $D(m_{i-1}, m_i, m_{i+1})$ pro model kapajícího disku a následné ověření tohoto modelu.
 \item Prověření vlivu \textit{konstantní} vs. \textit{nekonstantní} akrece $A$.
\end{itemize}

\end{frame}



\begin{frame}
	\begin{center}
		\huge Děkuji za pozornost.
	\end{center}
\end{frame}

%DOPLŇKOVÉ SNÍMKY

\begin{frame}{FDM - řešení rovnovážných tvarů visících kapek}
Rovnovážné tvary získáme numerickým řešením soustavy

\begin{equation*}
     \begin{aligned}
       		& \frac{dr}{ds} = \sin \theta  \\
       		& \frac{dz}{ds} = - \cos \theta \\
       		& \frac{d\theta}{ds} = \frac{\cos \theta}{r} - z
     \end{aligned}
     \label{rovnovazne_system}
\end{equation*}
\hspace{15mm}

při zadaných počátečních podmínkách

\begin{equation*}
     \begin{aligned}
       		z(0) &= P_b \\
       		\theta(0) &= \pi / 2 \\
       		r(0) &= 1 \cdot 10^{-20}. 
     \end{aligned}
     \label{rovnovazne_pocatecni}
\end{equation*}

\end{frame}

\begin{frame}{FDM - vývoj systému v čase}

Soustavu pohybových rovnic získáme z Lagrangeových rovnic

\begin{equation*}
     \begin{aligned}
       		& \frac{\textrm{d}}{\textrm{d}t} \frac{\partial L}{\partial \dot{z}_j} = \frac{\partial L}{\partial z_j} + \frac{1}{2} \frac{\partial \dot{E}_k}{\partial \dot{z}_j},
     \end{aligned}
     \label{lagr_rovnice}
\end{equation*}
\hspace{15mm}

kde poslední člen vyjadřuje disipativní funkci, která je dána rovnicí

\begin{equation*}
     \begin{aligned}
       		& \dot{E}_k = \sum_{j=1}^M \frac{(\dot{z}_j - \dot{z}_{j-1})}{(z_j - z_{j-1})}\Delta \xi_j.
     \end{aligned}
     \label{dsp}
\end{equation*}

Lagrangián systému má tvar

\begin{equation*}
     \begin{aligned}
       		& L = E_k - U_g - U_{\Gamma},
     \end{aligned}
     \label{lagrangian}
\end{equation*}

kde $E_k$ vyjadřuje celkovou kinetickou energii kapky, $U_g$ její celkovou potenciální energii a $U_{\Gamma}$ její energii povrchovou.

\end{frame}

\begin{frame}{FDM - vývoj systému v čase}
	Kinetická energie $E_k$ je součtem kinetických energií všech disků
	
	\begin{equation*}
     \begin{aligned}
       		& E_k = \frac{1}{2} \sum\limits_{j=1}^M  \Delta \xi_j \dot{z}_j^2.
     \end{aligned}
     \label{e_kin}
	\end{equation*}	
	
	Potenciální energie $U_g$ je součtem potenciálních energií všech disků
	
	\begin{equation*}
     \begin{aligned}
       		& U_g = - g \sum\limits_{j=1}^M \Delta \xi_j z_j.
     \end{aligned}
     \label{e_pot}
	\end{equation*}	
	
	Povrchovou energii aproximujeme jako
	
	\begin{equation*}
     \begin{aligned}
       		& U_{\Gamma} = \Gamma S.
     \end{aligned}
     \label{e_pov}
	\end{equation*}	
	
	Celkovou plochu kapky $S$ určíme jako
	
	\begin{equation*}
     \begin{aligned}
       		& S~= S_1(r_j, r_{j+1}, z_j, z_{j+1}) + \sum_{j=2}^{M-1} S_j(r_j, r_{j+1}, z_{j-1}, z_j, z_{j+1}) + S_M(r_M).
     \end{aligned}
     \label{e_pov_2}
	\end{equation*}	
	
\end{frame}

\begin{frame}{FDM - vývoj systému v čase}
	Po úpravách získáme soustavu pohybových rovnic jednotlivých disků
	
	\begin{equation*}
	\begin{aligned}
     	\dot{z}_0 &= v_0 \\
     	\dot{v}_0 &= 0 \\
     	\dot{z}_j &= v_j \\
       	\dot{v}_j &= \frac{1}{\Delta \xi_j} \left( \frac{\partial L}{\partial z_j} + \frac{1}{2} \frac{\partial \dot{E}_k}{\partial v_j} \right),
     \end{aligned}
     \label{aj_4}
	\end{equation*}

\vspace{3mm}	
	
kterou řešíme numericky s využitím Dormand--Princovy metody s adaptivním krokem.

\end{frame}

\begin{frame}{FDM - řešení rovnovážných tvarů visících kapek}

Změna poloměru $r_a$ (snížení na $r_a = 0,5$) se projeví změnou závislosti $V = f(P_b)$ a tím pádem změnou intervalu stabilních tvarů, který už není definován jako rozsah $P_b$. Tentokrát jako stabilní lze označit tvary na křivce před dosažením kritického objemu $V_c = 2,39$.

\begin{center}
	\includegraphics[width=11cm]{img/volume_P_faucet_2.png}
\end{center}

\end{frame}

\begin{frame}{MSMM - určení maximálního Lyapunovova exponentu}

Nástrojem \textbf{lyap\_k} z programového balíku Tisean určíme maximální Lyapunovův exponent, který je v případě $v_0 = 0,146$ kladný, což ukazuje na chaotické chování systému.

	\begin{center}
		\includegraphics[width=11cm]{img/graf.png}
	\end{center}

\end{frame}

\begin{frame}{Model kapajícího disku z MSMM}

MSMM můžeme modifikovat do podoby prstence složeného z $N$ buněk, u kterých přítok hmoty bude vyjádřen jako součet konstantní akrece $A$ a difuze $D$ závislé na gradientu hmoty mezi sousedními buňkami. Soustava rovnic popisující model kapajícího disku má tvar

	\begin{equation*}
	\begin{aligned}
		\dot{z}_1 &= v_1 \\
     	\dot{v}_1 &= g - \frac{1}{m_1} \left[ kz_1 + \gamma v_1 + v_1 A~+ v_1 D(m_{N}, m_{1}, m_{2})  \right] \\
     	...\\
     	\dot{z}_i &= v_i \\
     	\dot{v}_i &= g - \frac{1}{m_i} \left[ kz_i + \gamma v_i + v_i A~+ v_i D(m_{i-1}, m_{i}, m_{i+1})  \right] \\
     	...\\
     	\dot{z}_N &= v_N \\
     	\dot{v}_N &= g - \frac{1}{m_N} \left[ kz_N + \gamma v_N + v_N A~+ v_N D(m_{N-1}, m_{N}, m_{1})  \right].
     \end{aligned}
     \label{msm_disk_4}
	\end{equation*}
\end{frame}
		
\end{document}