#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import stats

class cor_dim:
    def __init__(self):
        pass
    
    def get_data(self, target):
        
        f = open(target, "r")
        lines = f.read().split("\n")
        f.close()
        
        data = []
        
        tmp = []
        for line in lines:
            if(re.search(r"^#", line) == None and len(line) >= 3):
                d = line.split(" ")
                tmp.append([float(d[0]), float(d[1])])
            else:
                if(len(tmp) > 0):
                    data.append(np.array(tmp))
                tmp = []
        
        return data
    
    def const(self, x, a):
        return a * np.ones_like(x)
    
    def fit(self, data):      
        x = data[1][:,0]
        y = data[1][:,1]
        i = 2
        while(i < len(data)):
            x = np.concatenate((x, data[i][:,0]), axis=0)
            y = np.concatenate((y, data[i][:,1]), axis=0)
            i += 1
        
        #omezeni na stred grafu
        x = x[np.where(x < 9.0)]
        y = y[np.where(x < 9.0)]
        x = x[np.where(x > 0.1)]
        y = y[np.where(x > 0.1)]
        
        popt, pcov = curve_fit(self.const, x, y)
        print popt, pcov
        
        return x, y, popt
    
    def plot(self, data, x, popt):
        fig = plt.figure(figsize=(19.2, 10.2))
        ax1 = fig.add_subplot(1, 1, 1)
        
        i = 1
        while(i < len(data)):
            d = data[i]
            ax1.plot(d[:, 0], d[:, 1], color="black", linestyle="-", marker="")
            i += 1
        
        #primka fitu
        ax1.plot(x, self.const(x, popt[0]), color="red", linestyle="-")
        
        ax1.grid()
        ax1.set_xscale("log")
        
        ax1.set_xlabel("$R$", fontsize=30)
        ax1.set_ylabel("$d_C$", fontsize=30)
        
        plt.savefig("plot.png")
        #plt.show()
    
if(__name__ == "__main__"):
    
    cor = cor_dim()
    
    data = cor.get_data("cor_dim.dat.d2")
    
    x, y, popt = cor.fit(data)
    
    cor.plot(data, x, popt)